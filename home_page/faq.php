<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Notum Team">

    <!--favicon icon-->
    <link rel="icon" type="image/png" href="images/favicon.png">

    <title>Notum News</title>


    <!--common style-->

    
<link href='http://fonts.googleapis.com/css?family=Abel|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic,200italic,200' rel='stylesheet' type='text/css'>
<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/linea-icon.css" rel="stylesheet">
    <link href="css/shortcodes/featured-box.css" rel="stylesheet">
    <link href="css/shortcodes/footer.css" rel="stylesheet">
    <link href="css/shortcodes/heading.css" rel="stylesheet">
    <link href="css/shortcodes/subscribe.css" rel="stylesheet">
    <link href="css/shortcodes/team.css" rel="stylesheet">
    <link href="css/shortcodes/buttons.css" rel="stylesheet">
    <link href="css/shortcodes/post.css" rel="stylesheet">
	<link href="css/shortcodes/toggle-accordion.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!--  preloader start -->
<div id="tb-preloader">
    <div class="tb-preloader-wave"></div>
</div>
<!-- preloader end -->


<div class="wrapper">

        <!--header start-->
        <header id="header" class=" header-full-width  ">

            <div class="header-sticky dark-header">

                <div class="container">
                    <div id="massive-menu" class="menuzord">

                        <!--logo start-->
                        <a href="index.html" class="logo-brand">
                            <img class="retina" src="images/logo.png" alt=""/>
                        </a>
                        <!--logo end-->

                        <!--mega menu start-->
                        <ul class="menuzord-menu pull-right">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="about.html#contact">Contact</a></li>
                            <li><a href="http://notumnews.tumblr.com/">Blog</a></li>


                            <li  class="nav-icon nav-divider">
                                <a href="javascript:void(0)">|</a>
                            </li>

                            <li><a href="http://app.notumnews.com/login">Login</a>
                            </li>

                        </ul>
                        <!--mega menu end-->

                    </div>
                </div>
            </div>

        </header>
        <!--header end-->

        <!--body content start-->
<section class="body-content">

  <div class="page-content">
                <div class="container">
                    <div class="row">
                        <div class="text-center" style="padding-bottom:50px;">
                            <h2 style="color:#d2213d;">F.A.Q.</h2>
                        </div>

                  </div>
                        <div class="col-md-6 col-md-offset-3">
<?


include "faq_config.php";

// SHOW ADMIN HEADER
//echo $admin_info[header];

// IF USER HAS CLICKED ON A QUESTION
if(isset($_GET['q_id'])) { $q_id = $_GET['q_id']; }

if($q_id != "") {
$question_query = mysql_query("SELECT * FROM faq_questions WHERE q_id='$q_id'");
$question = mysql_fetch_assoc($question_query);
if(mysql_num_rows($question_query) == 0) {
echo "<h2>An error has occurred:</h2>
You are attempting to view an FAQ question that does not exist.
";
}

echo "
<h2>$question[question]</h2>
$question[answer]
<form action='faq.php' method='POST'>
<input type='submit' value='Back to FAQ'>
</form>
";


} else {



// IF USER IS LOOKING AT THE MAIN PAGE
$totalcount = 0;
$faq_questions = mysql_query("SELECT * FROM faq_questions");

$faqcat = mysql_query("SELECT * FROM faq_categories ORDER BY c_order");
$num_of_kitties = mysql_num_rows($faqcat);
while($faqcat_info = mysql_fetch_assoc($faqcat)) {
$questions = mysql_query("SELECT q_id, c_id, question FROM faq_questions WHERE c_id='$faqcat_info[c_id]' ORDER BY q_order");


// SHOW CATEGORY NAME, IF IT CONTAINS QUESTIONS AND CATEGORY NAMES ARE TURNED ON
if(mysql_num_rows($questions) > 0) {
if($admin_info[showcats] == 1) {
echo "
<h2>$faqcat_info[category]</h2>
";
}
}


// SHOW QUESTIONS
$count = 0;
while($question = mysql_fetch_assoc($questions)) {
$totalcount++;
$count++;

// SHOW NUMBERS IF ENABLED
if($admin_info[shownumbers] == 1) {
if($admin_info[showcats] == 1) { echo "$count. "; } else { echo "$totalcount. "; }
}

echo "<a href='faq.php?q_id=$question[q_id]'>$question[question]</a><br>";
}


// ADD SPACE AFTER THIS CATEGORY
if(mysql_num_rows($questions) != 0 AND $admin_info[showcats] == 1) {
echo "<br>";
}

}
}



// SHOW HTML FOOTER
echo $admin_info[footer];


?>

					  </div>
    </div>

        </div>
        </section>
        <!--body content end-->

        <!--footer 1 start -->
        <footer id="footer" class="dark">
            <div class="primary-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="#" class="m-bot-20 footer-logo">
                                <img class="retina" src="images/logo-dark.png" alt=""/>
                            </a>
                            <p>INFORMATION. COMMUNICATION. INNOVATION.</p>
                            
                        </div>
                        <div class="col-md-3">
                            <h5 class="text-uppercase">Notum Benefits</h5>
                            <ul class="f-list">
                                <li><a href="company.html">Company Benefits</a></li>
                                <li><a href="reporter.html">Reporter Benefits</a></li>
                            </ul>
                        </div>
                                              <div class="col-md-2">
                            <h5 class="text-uppercase">Information</h5>
                            <ul class="f-list">
                                  <li><a href="about.html">About</a></li>
                                <li><a href="careers.html">Career</a></li>
                                <li><a href="cookie.html">Cookie</a></li>  
                                <li><a href="privacypolicy.html">Privacy Policy</a></li>
                                <li><a href="conditions.html">Conditions</a></li>
                                <li><a href="about.html#contact">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2">  
                          <h5 class="text-uppercase">Functionality</h5>
                          <ul class="f-list">
                                <li><a href="status.html">Platform Status</a></li>
                                <li><a href="faq.php">F.A.Q.</a></li>
                                <li><a href="jblog.html">Reporter blog</a></li>
                                <li><a href="forum.html">Help Forum</a></li>
                            </ul>
                          </div>
                          <div class="col-md-2">  
                          <h5 class="text-uppercase">Media & Partners</h5>
                            <ul class="f-list">
                               <li><a href="media.html">Media</a></li>
                               <li><a href="partners.html">Partners</a></li>
                               <li><a href="events.html">Events</a></li>
                              </ul>
                          </div>

                    </div>
                </div>
            </div>

            <div class="secondary-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <span class="m-top-10">Copyright 2015 Notum News | All Rights Reserved</span>
                        </div>
                        <div class="col-md-6">
                            <div class="social-link circle pull-right">
                                <a href="https://www.facebook.com/notumnews/?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/notumnews" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a href="https://www.instagram.com/notumnews/" target="_blank"><i class="fa fa-instagram"></i></a>
                                <a href="https://www.youtube.com/channel/UCc4kRsgTxE6Z_CQ5n0lcf5g" target="_blank"><i class="fa fa-youtube"></i></a>
                                <a href="https://www.linkedin.com/company/10087745?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A10087745%2Cidx%3A2-1-2%2CtarId%3A1450147677007%2Ctas%3Anotum" target="_blank"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--footer 1 end-->
    </div>


    <!-- Placed js at the end of the document so the pages load faster -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/menuzord.js"></script>
    <script src="js/jquery.isotope.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/visible.js"></script>
    <script src="js/smooth.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/imagesloaded.js"></script>
    <!--common scripts-->
    <script src="js/scripts.js?8"></script>



</body>
</html>

